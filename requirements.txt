imcrypt==1.2
imutils==0.5.3
numpy==1.16.0
opencv-python==4.2.0.34
pycrypto==2.6.1
pycryptodome==3.9.7
pyzmq==19.0.0
